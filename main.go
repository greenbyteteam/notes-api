package main

import (
	"api/storage"
	"api/utils"
	"api/views"

	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
	// "github.com/zserge/webview"
)

func enableLocalStatic(app *gin.Engine) {
	app.Use(static.Serve("/", static.LocalFile("./static", true)))
}

func enableBfsStatic(app *gin.Engine) {
	app.Use(static.Serve("/", BinaryFileSystem("static")))
}

func main() {

	app := gin.Default()

	storage.Init("./notes.db")

	// enableLocalStatic(app)
	enableBfsStatic(app)

	// Setup route group for the API v1
	api := app.Group("/api/v1")

	// define api routes
	api.GET("/", views.HealthStatus)
	api.GET("/notes", views.NotesCollection)
	api.POST("/notes", views.CreateNote)
	api.PUT("/notes", views.UpdateNotes)
	api.DELETE("/notes/:id", views.DeleteNote)

	utils.OpenInBrowser("http://localhost:8080/")
	// go webview.Open("Notes", "http://localhost:8080/", 1920, 1200, true)
	app.Run(":8080")
}
