======================================
**Notes** (REST API with UI Prototype)
======================================


compile & run:
--------------

1. Clone project repo to your ``$GOPATH/src`` directory 
2. Go to project directory: ``cd $GOPATH/src/notes-api`` 
3. *(only first time)* Install project dependencies: ``go get -v -d ./...`` 
4. *(only first time)* Install frontend dependencies: ``cd frontend/; install_frontend_deps; cd ../`` 
5. Compile projects static files: ``go-bindata static/...`` 
6. Compile project: ``go build -o notes-api -a -v .`` 
7. Run: ``./notes-api`` 

----

developing process:
-------------------

* go to project directory: ``cd $GOPATH/src/notes-api``
* prepare environment: ``source .env``
* *(only first time)* install project dependencies: ``go get -v -d ./...``
* *(only first time)* install frontend dependencies: ``cd frontend/; install_frontend_deps; cd ../``
* compile projects static files: ``go-bindata static/...``
* compile frontend: ``build_frontend`` (check *.env* file for aliases)
* run api with: ``go run main.go``

