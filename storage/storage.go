package storage

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

//Init initialize sqlite3 database
func Init(dbPath string) {
	var err error
	db, err = sql.Open("sqlite3", dbPath)
	// defer db.Close()
	panicIfErr(err)

	sqlStmt := `
	CREATE TABLE IF NOT EXISTS notes (
		id VARCHAR(60) UNIQUE PRIMARY KEY,
		title VARCHAR(100) NULL,
		text TEXT NULL,
		created DATETIME DEFAULT CURRENT_TIMESTAMP,
		color VARCHAR(8) NULL,
		is_editable BOOLEAN DEFAULT 1,
		grid_key VARCHAR(120) NOT NULL,
		grid_is_draggable BOOLEAN DEFAULT 1,
		grid_is_resizible BOOLEAN DEFAULT 1,
		grid_width INTEGER NULL,
		grid_height INTEGER NULL,
		grid_x INTEGER NULL,
		grid_y INTEGER NULL
    );
	`
	_, err = db.Exec(sqlStmt)
	panicIfErr(err)
}

//AddNote is used to insert new DB record
func AddNote(note *Note) {
	query, err := db.Prepare("INSERT INTO notes (id, title, text, color, is_editable, grid_key, grid_is_draggable, grid_is_resizible, grid_width, grid_height, grid_x, grid_y, created) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP)")
	panicIfErr(err)
	_, err = query.Exec(
		note.ID,
		note.Title,
		note.Text,
		note.Color,
		note.ContentEditable,
		note.Grid.Key,
		true,
		true,
		note.Grid.Width,
		note.Grid.Height,
		note.Grid.X,
		note.Grid.Y,
	)
	panicIfErr(err)
}

//UpdateNote is used to update DB record
func UpdateNote(note *Note) {
	query, err := db.Prepare("UPDATE notes SET title=?, text=?, color=?, is_editable=?, grid_width=?, grid_height=?, grid_x=?, grid_y=? where id=?")
	panicIfErr(err)
	_, err = query.Exec(
		note.Title,
		note.Text,
		note.Color,
		note.ContentEditable,
		note.Grid.Width,
		note.Grid.Height,
		note.Grid.X,
		note.Grid.Y,
		note.ID,
	)
	panicIfErr(err)
}

//DeleteNote is used to delete DB record
func DeleteNote(noteID string) {
	_, err := db.Exec(fmt.Sprintf("delete from notes where id = '%s'", noteID))
	panicIfErr(err)
}

//FindNotes is used to select DB records
func FindNotes() []Note {
	rows, err := db.Query("SELECT id, title, text, datetime(created), color, is_editable, grid_key, grid_is_draggable, grid_is_resizible, grid_width, grid_height, grid_x, grid_y FROM notes ORDER BY datetime(created) DESC")
	defer rows.Close()
	panicIfErr(err)

	var result []Note

	for rows.Next() {
		var note Note
		err = rows.Scan(
			&note.ID,
			&note.Title,
			&note.Text,
			&note.TimeStamp,
			&note.Color,
			&note.ContentEditable,
			&note.Grid.Key,
			&note.Grid.IsDraggable,
			&note.Grid.IsResizable,
			&note.Grid.Width,
			&note.Grid.Height,
			&note.Grid.X,
			&note.Grid.Y,
		)
		panicIfErr(err)
		result = append(result, note)
	}
	return result
}

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}
