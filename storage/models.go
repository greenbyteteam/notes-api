package storage

//NoteGrid is basic json structure
type NoteGrid struct {
	Key         string `json:"i"`
	IsDraggable bool   `json:"isDraggable" binding:"exists"`
	IsResizable bool   `json:"isResizable" binding:"exists"`
	Width       int    `json:"w"`
	Height      int    `json:"h"`
	X           int    `json:"x"`
	Y           int    `json:"y"`
}

//Note is basic json structure
type Note struct {
	Grid            NoteGrid `json:"grid" binding:"required"`
	ID              string   `json:"id" binding:"required,uuid"`
	Title           string   `json:"title"`
	Text            string   `json:"text"`
	TimeStamp       string   `json:"timeStamp"`
	ContentEditable bool     `json:"contentEditable"`
	Color           string   `json:"color"`
}
