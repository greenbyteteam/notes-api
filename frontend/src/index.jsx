import React from 'react';
import ReactDOM from 'react-dom';
import NotesApp from './components/app';

ReactDOM.render(<NotesApp />, document.getElementById('app'));
