import React from 'react';
import Stickies from 'react-stickies';


function cloneOf(obj) {
    return JSON.parse(JSON.stringify(obj));
}


class NotesApp extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            notes: [],
            isLoaded: false,
            error: null,
            showTape: true,
            showTitle: true,
            showFooter: true,
            colors: ['#FFFFFF'],
            showCustomColors: false,
        };
        this.onChange = this.onChange.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentDidMount() {
        fetch("/api/v1/notes")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        notes: result.items || [],
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error: error,
                    });
                }
            );
    }

    onChange(notes, state) {
        this.setState({ // Update the notes state
            notes: notes,
        });
        console.log(`Change state: ${state}`);
        if ( ['update', 'layout'].includes(state) ) {
            const items = cloneOf(notes);
            items.map(i => {
                delete i.editorState;
            });
            fetch("/api/v1/notes",
                {
                    headers: {
                        'accept': 'application/json',
                        'content-type': 'application/json',
                    },
                    method: "PUT",
                    body: JSON.stringify({items: items}),
                })
                .then(resp => resp.json())
                .then(data => console.log(data))
                .catch(resp => console.log(resp));
        }
    }

    onAdd(originalNote) {
        let note = cloneOf(originalNote);
        if (note.editorState !== undefined) {
            delete note.editorState;
        }
        console.log(`Added new note: `, note);
        fetch("/api/v1/notes",
            {
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                },
                method: "POST",
                body: JSON.stringify(note),
            })
            .then(resp => resp.json())
            .then(data => console.log(data))
            .catch(resp => console.log(resp));
    }

    onDelete(originalNote) {
        let note = cloneOf(originalNote);
        if (note.editorState !== undefined) {
            delete note.editorState;
        }
        console.log(`Deleted note: `, note);
        fetch(`/api/v1/notes/${note.id}`,
            {
                headers: {
                    'accept': 'application/json',
                    'content-type': 'application/json',
                },
                method: "DELETE",
                body: {},
            })
            .then(resp => resp.json())
            .then(data => console.log(data))
            .catch(resp => console.log(resp));
    }

    render() {
        const { notes, isLoaded, error } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <Stickies
                    notes={this.state.notes}
                    tape={this.state.showTape}
                    style={{float: 'left' }}
                    colors={this.state.showCustomColors ? this.state.colors : undefined}
                    title={this.state.showTitle}
                    footer={this.state.showFooter}
                    onAdd={this.onAdd}
                    onDelete={this.onDelete}
                    onChange={this.onChange}
                />
            );
        }
    }

}

export default NotesApp;
