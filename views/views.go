package views

import (
	"api/storage"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

//HealthStatus view
func HealthStatus(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"status": "LIVE",
		"date":   time.Now(),
	})
}

//NotesCollection view
func NotesCollection(c *gin.Context) {
	notes := storage.FindNotes()
	c.JSON(
		http.StatusOK,
		gin.H{
			"status": "OK",
			"items":  notes,
		},
	)
}

//CreateNote view
func CreateNote(c *gin.Context) {
	newNote := storage.Note{}
	if err := c.ShouldBindJSON(&newNote); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "ERROR",
			"message": err.Error(),
		})
		return
	}
	storage.AddNote(&newNote)
	c.JSON(http.StatusOK, gin.H{
		"status":  "OK",
		"message": fmt.Sprintf("Note id=%s was created", newNote.ID),
	})
}

//UpdateNotes view
func UpdateNotes(c *gin.Context) {
	type notesForUpdate struct {
		Items []storage.Note `json:"items" binding:"required"`
	}
	notes := notesForUpdate{}
	if err := c.ShouldBindJSON(&notes); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "ERROR",
			"message": err.Error(),
		})
		return
	}
	for _, n := range notes.Items {
		storage.UpdateNote(&n)
	}
	c.JSON(http.StatusOK, gin.H{
		"status":  "OK",
		"message": "Notes collection updated",
	})
}

//DeleteNote view
func DeleteNote(c *gin.Context) {
	noteID := c.Param("id")
	storage.DeleteNote(string(noteID))
	c.JSON(http.StatusOK, gin.H{
		"status":  "OK",
		"message": fmt.Sprintf("Note id=%s was deleted", noteID),
	})
}
